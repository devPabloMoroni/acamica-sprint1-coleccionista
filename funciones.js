//Se define constante sino cada vez que se presiona el boton de agregar
//se crea nuevamente y se pierden los datos
const arrayVehiculos = [];

function agregarVehiculo(){
    //Recibo dato desde el formulario con la marca
    marca = document.getElementById("marca").value;
    //Pregunto si existe el dato desde el formulario
    if(marca){
        //Si existe, agrego la marca al array
        arrayVehiculos.push(marca);
    }

    //Creo una variable para ir guardando todas las marcas
    agregados = "";
    //Recorro el arreglo de vehiculos
    for(vehiculo of arrayVehiculos){
        //Por cada vuelta voy agregando a la variable agregados, cada marca
        agregados += vehiculo + "<br>";
        //Escribo en el documento todos los vehiculos agregados
        document.getElementById("listadoVehiculos").innerHTML = agregados;
    }
    //Borro los datos de mi input
    document.getElementById("marca").value = "";
}

function eliminarPrimero(){
    //Uso la funcion shift para eliminar el primero
    arrayVehiculos.shift();
    agregados = "";
    //pregunto si el arreglo tiene elementos, sino que no muestre nada
    if(arrayVehiculos == 0){
        document.getElementById("listadoVehiculos").innerHTML = "";
    }
    else{
        //si tiene, lo recorro y muestro el contenido
        for(vehiculo of arrayVehiculos){
        agregados += vehiculo + "<br>";
        document.getElementById("listadoVehiculos").innerHTML = agregados;
        }
    }
}

//Idem que el anterior pero eliminando desde atras hacia adelante
function eliminarUltimo(){
    arrayVehiculos.pop();
    agregados = "";
    if(arrayVehiculos == 0){
        document.getElementById("listadoVehiculos").innerHTML = "";
    }
    else{
        for(vehiculo of arrayVehiculos){
            agregados += vehiculo + "<br>";
            document.getElementById("listadoVehiculos").innerHTML = agregados;
        }
    }
}